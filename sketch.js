const MAX_TIME = 20;
const RATE = 60; // limited by processor speed

let f;
let p;
let c;

let a = (a, b, f, ac) => {
    return (t) => {
        let d = (b - a);
        let s = (t) => 1 / (1 + Math.pow(Math.E, -20 * t + 10));
        if (ac == false) {
            s = (t) => t;
        }

        return f(d * s(t / d - a / d) + a);
    }
}

let b = (f, xPixelEquivalent, yPixelEquivalent) => {
    return (t) => {
        let r = f(t);
        return createVector(r.x * xPixelEquivalent, r.y * yPixelEquivalent);
    }
}

let genCurve = (x, y, p, q, xA, yA, c, ac) => {
    return new Curve(
        p,
        q,
        c,
        a(p, q, b((t) => createVector(x(t), y(t)), width / (2 * xA), height / (2 * yA)), ac)
    );
}

let font_CMU_UI;
function preload() {
  font_CMU_UI = loadFont('assets/cmunti.ttf');
}

function setup() {
  textFont(font_CMU_UI);
    let optimumSize = Math.min(windowHeight, windowWidth);
    createCanvas(optimumSize * 0.9, optimumSize * 0.9);
    // createCanvas(windowWidth, windowHeight);
    frameRate(RATE);
    let curves = [/*c_SemicCubicParabola(), c_Circle() , c_Parabola(), c_Root(),*/ c_Sinus(), c_Helix() ];

    p = new Plotter(curves, - 2 * PI, 50, MAX_TIME, 1 / RATE, 1);
}

function draw() {
    p.tick();
}

function c_SemicCubicParabola() {
    return genCurve((t) => t * t, (t) => t * t * t, -2, 2, 4, 16, color(255, 0, 0), false);
}

function c_Circle() {
    return genCurve((t) => cos(t), (t) => sin(t), 0, 2 * Math.PI, 1, 1, color(255, 255, 0));
}

function c_Parabola() {
    return genCurve((t) => t, (t) => t * t, -2, 2, 2, 4, color(255, 255, 255), false);
}

function c_Root() {
    return genCurve((t) => t, (t) => Math.sqrt(t), 0, 2, 2, Math.sqrt(2), color(0, 255, 255));
}

function c_Sinus() {
    return genCurve((t) => t, (t) => sin(t), -2 * Math.PI, 2 * Math.PI, 2 * Math.PI, 2, color(0, 255, 255), false);
}

function c_Helix() {
    return genCurve((t) => 1 * cos(t) * Math.exp(0.25 * t), (t) => 1 * sin(t) * Math.exp(0.25 * t), 0, 2 * PI * 7, Math.exp(0.25 * 2 * PI * 7), Math.exp(0.25 * 2 * PI * 7), color(255, 0, 255));
}

function drawGrid() {
    push();
    background(0);
    let c = color(200, 200, 200);
    stroke(c);
    strokeWeight(2);

    line(0, 1, width, 1);
    line(width - 1, 1, width - 1, height - 1);
    line(1, 1, 1, height);
    line(width - 1, height - 1, 1, height - 1);

    drawArrow(createVector(-width / 2, 0), createVector(width / 2 - 1, 0), color("#FF"), false);
    drawArrow(createVector(0, -height / 2), createVector(0, height / 2 - 1), color("#FF"), false);

    pop();
}

class Curve {
    constructor(a, b, c, fn) {
        if (a > b) {
            let tmp = a;
            a = b;
            b = tmp;
        }

        this.a = a;
        this.b = b;

        if (fn && {}.toString.call(fn) === '[object Function]') {
            this.fn = fn;
        } else {
            throw 'fn is not a function so it does not define a curve.';
        }

        this.c = c;
    }

    apply(t) {
        t = float(t.toFixed(5)); // prevents rejection of legal t's which may result of not perfect divisions of Plotter's domain (.step)
        if ((t >= this.a) && (t <= this.b)) {
            let x = this.fn(t);

            if (!(x instanceof p5.Vector || isNaN(x))) {
                throw 'fn must return a p5.Vector or NaN.';
            }

            if (isNaN(x.x) || isNaN(x.y)) {
                return NaN;
            } else {
                return x;
            }
        } else {
            return NaN;
        }
    }
}

class Plotter {
    // time limit := time until the domain has been completely traversed
    // tick_aquivalent := time passed from one to the next tick
    constructor(curves, a, b, time_limit, tick_aquivalent) {
        tick_aquivalent = abs(tick_aquivalent);

        if (tick_aquivalent > time_limit) {
            throw 'Cannot compute curve when a tick takes more time then Plotter is allowed to allocate.';
        }

        if (a > b) {
            // swap
            let tmp = a;
            a = b;
            b = tmp;
        }

        this.a = a;
        this.b = b;
        this.f = 1;
        this.step = (abs(this.b - this.a) / time_limit * tick_aquivalent);


        if (!Array.isArray(curves)) {
            curves = [curves];
        }

        for (let i = 0; i < curves.length; i++) {
            if (!(curves[i] instanceof Curve)) {
                throw 'All curves must be instances of class Curve.';
            }
        }

        if (!this.curves) {
            this.curves = [];
        }

        this.curves = this.curves.concat(curves);

        if (f == null) {
            f = 1;
        }

        this.round = 0;

        this.reset();

        console.log('Curves to process:', this.curves);
        console.log('Domain:', this.a, 'to', this.b);
        console.log('t-step per tick:', this.step);
    }

    reset() {
        this.t = this.a;
        this.round = this.round + 1;
        this.curveValues = [];
    }

    tick() {
        drawGrid();
        drawArrow(createVector(0, 0), createVector(100 * cos((frameCount / 100) % (2 * Math.PI)), 100 * sin((frameCount / 100) % (2 * Math.PI))), color("#FF0000"), true);
        if (this.t >= this.a && this.t <= this.b) {
            for (let i = 0; i < this.curves.length; i++) {
                let curve = this.curves[i];

                let value = curve.apply(this.t);

                if (this.curveValues[i] == null) {
                    this.curveValues[i] = [];
                }

                this.curveValues[i].push(value);
                this.drawSegments(this.curveValues[i], curve);
            }
            this.t += this.step;
        } else {
            this.reset();
        }

    }

    drawSegments(curveValues, curve) {
        push();
        translate(width / 2, height / 2);
        stroke(curve.c);
        strokeWeight(2);
        if (curveValues.length < 2) {
            return;
        }

        let x_1 = curveValues[0];
        let x_2;
        for (let i = 1; i < curveValues.length; i++) {
            x_2 = curveValues[i];

            if (Plotter.isDefined(x_2) && Plotter.isDefined(x_1)) {
                let x_1_s = mirrorAtX(this.scale(x_1));
                let x_2_s = mirrorAtX(this.scale(x_2));

                line(x_1_s.x, x_1_s.y, x_2_s.x, x_2_s.y);
            }

            x_1 = x_2;
        }

        fill(color(255, 0, 0));
        noStroke();
        let last = curveValues[curveValues.length - 1];
        let penultimate = curveValues[curveValues.length - 2];
        if (Plotter.isDefined(last) && Plotter.isDefined(penultimate)) {
          last = this.scale(last);
          penultimate = this.scale(penultimate);

          circle(mirrorAtX(last.x), mirrorAtX(last.y), 5);
          pop();
          drawArrow(penultimate,last,null,true, "v'");
          return;

        }
        pop();


    }

    static isDefined(v) {
        return (v !== null && v instanceof p5.Vector);
    }

    scale(v) {
        // console.log(v);
        v.x = (isFinite(v.x)) ? v.x : float(v.x.toFixed(5));
        v.y = (isFinite(v.y)) ? v.y : float(v.y.toFixed(5));

        let scaled = p5.Vector.mult(v, float(this.f.toFixed(5)));
        return scaled;
    }
}

function mirrorAtX(v) {
    return createVector(v.x, -1 * v.y);
}

function mouseMoved() {
    p.f = constrain(createVector(mouseX - width / 2, mouseY - height / 2).mag() / createVector(width / 2, height / 2).mag(), 0, 1);
}

function drawArrow(p, u, c, showInfo, name) {
    if (c == null) {
        c = color(255, 255, 255);
    }

    push();
    stroke(c);
    fill(c);

    translate(width / 2, height / 2);
    p = mirrorAtX(p);
    u = mirrorAtX(u);
    // printEvery(p + "/" + u, 50);
    line(p.x, p.y, u.x, u.y);
    noStroke();
    var angle = atan2(u.y - p.y, u.x - p.x); //gets the angle of the line
    translate(u.x, u.y); //translates to the destination vertex
    rotate(angle + HALF_PI); //rotates the arrow point

    let offset = 10;
    triangle(-5, 10, 5, 10, 0, 0); //draws the arrow point as a triangle
    p = mirrorAtX(p);
    u = mirrorAtX(u);
    if (showInfo === true){
        if(name == null) name = "v";
        text("α(" + name + "): " + int((180 * smoothAtan2(u.y - p.y, u.x - p.x)) / Math.PI) + "°", 5, 5);
        text("||" + name + "||: " + p5.Vector.sub(u, p).mag().toFixed(3), 5, 18);
    }
    pop();
}

function smoothAtan2(y, x) {
    if (y < 0) {
        return 2 * Math.PI + atan2(y, x);
    } else {
        return atan2(y, x);
    }
}

function printEvery(msg, n) {
    if ((frameCount % n) == 0) {
        console.log(msg);
    }
}
